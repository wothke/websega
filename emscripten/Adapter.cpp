/*
* This file adapts "Highly Theoretical" to the interface expected by my generic JavaScript player..
*
* Copyright (C) 2018 Juergen Wothke
*
* LICENSE
*
* This library is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or (at
* your option) any later version. This library is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>     /* malloc, free, rand */

#include <exception>
#include <iostream>
#include <fstream>

#include "MetaInfoHelper.h"
using emsutil::MetaInfoHelper;


#define BUF_SIZE	1024
#define TEXT_MAX	255
#define NUM_MAX	15

// see Sound::Sample::CHANNELS
#define CHANNELS 2
#define BYTES_PER_SAMPLE 2
#define SAMPLE_BUF_SIZE	1024


const char* info_texts[7];

char title_str[TEXT_MAX];
char artist_str[TEXT_MAX];
char game_str[TEXT_MAX];
char year_str[TEXT_MAX];
char genre_str[TEXT_MAX];
char copyright_str[TEXT_MAX];
char psfby_str[TEXT_MAX];


// interface to n64plug.cpp
extern	void ht_setup (void);
extern	void ht_boost_volume(unsigned char b);
extern	int32_t ht_get_samples_to_play ();
extern	int32_t ht_get_samples_played ();
extern	int32_t ht_get_sample_rate ();
extern	int ht_load_file(const char *uri, int16_t *output_buffer, uint16_t outSize);
extern	int ht_read(int16_t *output_buffer, uint16_t outSize);
extern	void ht_seek(double ms);

void ht_meta_set(const char * tag, const char * value)
{
	MetaInfoHelper *info = MetaInfoHelper::getInstance();

	// propagate selected meta info for use in GUI
	if (!strcasecmp(tag, "title"))
	{
		info->setText(0, value, "");
	}
	else if (!strcasecmp(tag, "artist"))
	{
		info->setText(1, value, "");
	}
	else if (!strcasecmp(tag, "album"))
	{
		info->setText(2, value, "");
	}
	else if (!strcasecmp(tag, "date"))
	{
		info->setText(3, value, "");
	}
	else if (!strcasecmp(tag, "genre"))
	{
		info->setText(4, value, "");
	}
	else if (!strcasecmp(tag, "copyright"))
	{
		info->setText(5, value, "");
	}
	else if (!strcasecmp(tag, "usfby"))
	{
		info->setText(6, value, "");
	}
}

namespace ht {
	class Adapter {
	public:
		Adapter() : _samplesAvailable(0)
		{
		}

		int loadFile(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: sampleRate, audioBufSize, scopesEnabled support not implemented (much is hardcoded in the DS emulator)

			ht_setup();
			return (ht_load_file(filename, _sampleBuffer, SAMPLE_BUF_SIZE) == 0) ? 0 : -1;
		}

		void teardown()
		{
			MetaInfoHelper::getInstance()->clear();
		}

		int setBoost(unsigned char boost)
		{
			ht_boost_volume(boost);
			return 0;
		}

		int getSampleRate()
		{
			return ht_get_sample_rate();
		}

		char* getSampleBuffer()
		{
			return (char*)_sampleBuffer;
		}

		long getSampleBufferLength()
		{
			return _samplesAvailable;
		}

		int getCurrentPosition()
		{
			return ((long long)ht_get_samples_played()) * 1000 / ht_get_sample_rate();
		}

		void seekPosition(int ms)
		{
			// fixme: the respective decode_seek seems to be garbage and it puts the emulator into an
			// inconsistent state - causing an invalid memory access
			
			ht_seek((double)ms);
		}

		int getMaxPosition()
		{
			return ((long long)ht_get_samples_to_play()) * 1000 / ht_get_sample_rate();
		}

		int genSamples()
		{
			_samplesAvailable =  ht_read((short*)_sampleBuffer, SAMPLE_BUF_SIZE);	// returns number of bytes

			if (_samplesAvailable <= 0)
			{
				_samplesAvailable = 0;
				return 1;
			}
			else
			{
				return 0;
			}
		}
	private:
		signed short _sampleBuffer[SAMPLE_BUF_SIZE * CHANNELS];
		int _samplesAvailable;
	};
};

ht::Adapter _adapter;



// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func

// --- standard functions
EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return 0;	/*there are no subsongs*/ }
EMBIND(const char**, emu_get_track_info())			{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms))				{ _adapter.seekPosition(ms);  }
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }


// --- add-on
EMBIND(int, emu_set_boost(int boost))				{ return _adapter.setBoost(boost); }
