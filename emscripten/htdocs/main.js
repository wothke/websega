let songs = [
		"Dreamcast Sound Format/Daisuke Ishiwatari/Guilty Gear X (Naomi)/ggx-66-00-01.minidsf",
		"Dreamcast Sound Format/Kohei Tanaka/Sakura Taisen 3 - Paris Wa Moeteiru Ka/sakutai3-11.dsf",
	];

class SEGADisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 	{ return "webSEGA";}
	getDisplaySubtitle() 	{ return "highly theoretical music..";}
	getDisplayLine1() { return this.getSongInfo().title +" ("+this.getSongInfo().artist+")";}
	getDisplayLine2() { return this.getSongInfo().copyright; }
	getDisplayLine3() { return ""; }
};

class SEGAControls extends BasicControls {
	constructor(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, current)
	{
		super(containerId, songs, doParseUrl, onNewTrackCallback, enableSeek, enableSpeedTweak, current);
	}

	_addSong(filename)
	{
		if (filename.indexOf(".dsflib") == -1)
		{
			this._someSongs.splice(this._current + 1, 0, filename);
		}
	}
};

class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new SEGABackendAdapter();
//		this._backend.setProcessorBufSize(2048);

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, {}];
				};

			this._playerWidget = new SEGAControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new SEGADisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0xf96900,0xffffff,0x343d9d,0xffffff], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}